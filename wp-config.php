<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'wpLarcherNicolasElectricite' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'K<WPDBb?u(HTS%U_77i[O.K>U$_$qMk[v@eYmbG>A`qIfP1K&CunUSw!6G;`IC{n' );
define( 'SECURE_AUTH_KEY',  ']c:}l{+BD%+3m&Sm~dD)USYxb,K7caG:8Ryd,-:L.6wi]^,AG|s79;ul.Fv?Bja2' );
define( 'LOGGED_IN_KEY',    ' wu/xs~uI!Cf =K]Bl<J9Ft~&57HEEkR^X+#pmgyL-g*A=,avb)`1ksfj:9[xA~n' );
define( 'NONCE_KEY',        '#A)Er*GUPQw0#n~|<e_aJ}2T?%-FvJ,2m)n>j8sD^_{&~SN@j@AR@y_{w{N^(xL/' );
define( 'AUTH_SALT',        'uRrLwvVpV1MGj^71NYZxh*Gv{U;Sx;So[0ScNaF$hGdwUR+q8#HC_2&x][?Yti6)' );
define( 'SECURE_AUTH_SALT', 'k#1PwEUgh0rRa3?smLiW}9nsPCO{i{v]t@%(-Y+HO{/adU}.}{sCvx2b;sR9V]2#' );
define( 'LOGGED_IN_SALT',   '1m<:;> PL*0 1Z:aP(RV99~$b@berK=,!Nlja/+hK.2_(#(GZtQqgjhDS@0]v}u)' );
define( 'NONCE_SALT',       'DY^HVB/BW?+jQBw8jSEcoc8$>xvD!`2+rfRPSKuJGP}81#k2D+oJ Y0prc:<%lrH' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
