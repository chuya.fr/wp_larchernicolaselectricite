<?php 
// require_once( __DIR__ . '/vendor/autoload.php' );
// $timber = new Timber\Timber();

// Add promote supported images
add_theme_support( 'post-thumbnails' );

// Add automatically the website title in the website header
add_theme_support( 'title-tag' );

add_action('wp_enqueue_scripts', 'insert_css_in_head');

function insert_css_in_head() {
    // Add CSS in general theme
    wp_register_style('style', get_bloginfo( 'stylesheet_url' ),'',false,'screen');
    wp_enqueue_style( 'style' );
}

add_action('wp_head','custom_favicon', 1);
function custom_favicon() {
    echo '<link rel="shortcut icon" type="image/x-icon" href="'. get_bloginfo('template_directory') .'/design/favicon.ico" />'."n";
    echo '<link rel="icon" type="image/x-icon" href="'. get_bloginfo('template_directory') .'/design/favicon.ico" />'."n";
}

register_nav_menu( 'primary', 'Primary Menu' );

