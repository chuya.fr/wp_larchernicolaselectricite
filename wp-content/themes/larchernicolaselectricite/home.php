<?php get_header(); ?>

	<?php the_post_thumbnail(); ?>

 	<h1><?php the_title(); ?></h1>

	<?php the_content(); ?>

	<?php endwhile; endif; ?>
<?php get_footer(); ?>

